#' Chart an economic time series given an xts.
#'
#' @export
#' @import ggplot2
#' @import labels
#' @import xts
#' @import xtsfunctions
#' @importFrom reshape2 dcast
#' @importFrom scales rescale_none
#' @importFrom stats na.omit start lm coef
#' @importFrom utils tail
#'
#' @param l An xts or a list of xts's that will be part of the chart.  The names of the l determine the labels.
#' @param title Chart title.
#' @param subtitle Chart subtitle.
#' @param time For bar charts, what time period should we use.  If NULL, the chart is a line chart.  The last value is determined by Inf.
#' @param ymin Y-axis minimum limit.
#' @param ymax Y-axis maximum limit.
#' @param sec_axis This enables a secondary axis.
#' @param sec_reg This uses a regression to fit the secondary axis rather than a simple minmax
#' @param zero_line Enabled by default, this draws a line at zero.
#' @param rebase This rebases the time series to the start date
#' @param labels The default labels for the y-axis.  Usually done by labels::econ but can also be labels::USD.
#' @param labels2 Disabled if sec_axis is FALSE.  The label function for the secondary axis.
#' @param sources Additional sources besides the ones passed in through the xts list.
#' @param show_plot If TRUE (default), the function will automatically show the plot.
#' @param save_plot If TRUE (default), the function will automatically save the plot in the global environment with a g prefix.
#' @param ... additional functions passed to \code{\link{transform_series}} documented below:
eplot <- function(l, title = NULL, subtitle = NULL, time = NULL, ymin = NA, ymax = NA, sec_axis = FALSE, sec_reg = FALSE, zero_line = TRUE, rebase = FALSE, labels = labels::auto_label, labels2 = labels::auto_label, sources = NULL, show_plot = getOption("show_plot", TRUE), save_plot = getOption("save_plot", TRUE), ...) {

    #cast to list
    if(xts::is.xts(l)) {
        if(ncol(l) > 1) {
            xts::xtsAttributes(l) <- NULL
            l <- as.list(l)
        } else {
            l <- list(l)
        }
    } else if(!is.list(l)) {
        stop("Please provide a list or xts to eplot", call. = FALSE)
    }
    
    stopifnot(all(sapply(l, xts::is.xts)))

    plot_names <- names(l)
    #deal with plot names

    if(is.null(plot_names)) {
        xts_names <- sapply(l, attr, "name")
        if(is.character(xts_names) && !any(duplicated(xts_names))) {
            plot_names <- xts_names
        } else {
            plot_names <- make.names(sapply(l, names), unique = TRUE)
        }
    }

    stopifnot(is.character(plot_names))
    stopifnot(length(l) == length(plot_names))

    #deal with plot_colors

    plot_colors <- sapply(l, function(x) {
        country <- attr(x, "country")
        if(is.null(country)) country <- names(x)
        country_color(country)
    })

    if(any(is.na(plot_colors)) || any(duplicated(plot_colors))) plot_colors <- NULL

    #deal with title

    if(is.null(title)) {
        save_plot <- FALSE
        title <- paste(plot_names, collapse = ", ")
    }

    #deal with subtitle

    arguments <- as.list(match.call())
    subtitle <- paste(unlist(c(list(subtitle), sapply(names(arguments), function(x) {
        list(ma12 = "12mma",
             ma6 = "6mma",
             ma3 = "3mma",
             yoy = "yoy",
             qoq = "qoq",
             mom = "mom",
             smsar = "6msar",
             sa = "sa",
             gdp = "% gdp",
             rebase = "index")[[x]]
    }))), collapse = "; ")

    if(length(intersect(names(arguments), c("yoy", "qoq", "mom", "gdp"))) > 0) labels <- labels::percent

    #deal with captions

    sources <- unlist(c(sources, sapply(l, attr, "source")))
    caption <- ifelse(length(sources) > 0, paste0("Source: ", paste(unique(sort(sources)), collapse = ", ")), "")

    #deal with transformation

    l <- xtsfunctions::xts_compact(l)
    l <- lapply(l, transform_series, ...)

    #deal with rebasing

    if(rebase) {
        rebase_date <- as.POSIXlt(max(sapply(l, function(x) stats::start(stats::na.omit(x)))), origin = "1970-01-01")
        l <- lapply(l, function(x) x / as.numeric(utils::tail(x[zoo::index(x) <= rebase_date], 1)) * 100)
    }

    if(is.null(time)) {

        df <- do.call(rbind, lapply(1:length(l), function(i) {
            data.frame(time = zoo::index(l[[i]]),
                       variable = ordered(plot_names[i]),
                       value = as.numeric(l[[i]]),
                       row.names = NULL)
        }))

        if(!(sec_axis == FALSE)) {

            if(sec_axis == TRUE) sec_axis <- plot_names[2]
            if(!(sec_axis %in% plot_names)) stop(paste0("Could not find the second axis in the data."), call. = FALSE)

            vals1 <- df[df$variable == sec_axis, "value"]
            vals2 <- df[df$variable != sec_axis, "value"]

            #either use regression or simple limits
            if(sec_reg) {

                dc <- reshape2::dcast(df, time ~ variable, value.var = "value")
                names(dc) <- c("T", "X", "Y")

                fit <- stats::lm(Y ~ X, data = dc)

                m <- stats::coef(fit)[2]
                b <- stats::coef(fit)[1]

            } else {

                ymin1 <- min(vals1, na.rm = TRUE)
                ymax1 <- max(vals1, na.rm = TRUE)
                ymin2 <- ifelse(is.na(ymin), min(vals2, na.rm = TRUE), ymin)
                ymax2 <- ifelse(is.na(ymax), max(vals2, na.rm = TRUE), ymax)

                m <- (ymax1 - ymin1) / (ymax2 - ymin2)
                b <- ymax1 - m * ymax2

            }

            plot_names[plot_names == sec_axis] <- paste0(sec_axis, " [RHS]")

            df[df$variable == sec_axis, "value"] <- (vals1 - b) / m

            sec_axis <- ggplot2::sec_axis(trans = ~ . * m + b, labels = labels2)

        } else {
            sec_axis <- ggplot2::waiver()
        }

        g <- ggplot2::ggplot(df, ggplot2::aes(x = .data$time, y = .data$value, fill = .data$variable, color = .data$variable))
        g <- g + ggplot2::geom_path(na.rm = TRUE)
        g <- g + ggplot2::scale_y_continuous(limits = c(ymin, ymax), labels = labels, sec.axis = sec_axis)

        if(length(l) == 1) {
            g <- g + ggplot2::guides(color = "none", fill = "none")
        }

        if(rebase) g <- g + ggplot2::geom_hline(yintercept = 100)

    } else {

        categories <- time
        categories[categories == Inf] <- "Latest"

        xtime <- as.character(time)
        xtime[time == Inf] <- "/"

        df <- do.call(rbind, lapply(1:length(l), function(i) {
            data.frame(time = categories,
                       variable = ordered(plot_names[i]),
                       value = as.numeric(sapply(xtime, function(z) utils::tail(stats::na.omit(l[[i]][z]), 1))),
                       row.names = NULL)
        }))

        if(length(time) > 1) {
            g <- ggplot2::ggplot(df, ggplot2::aes(x = .data$time, y = .data$value, fill = .data$variable, color = .data$variable))
            g <- g + ggplot2::geom_bar(stat = "identity") + ggplot2::facet_wrap(~variable, nrow = 1)
        } else {
            g <- ggplot2::ggplot(df, ggplot2::aes(x = .data$variable, y = .data$value, fill = .data$variable, color = .data$variable))
            g <- g + ggplot2::geom_bar(stat = "identity")
        }

        g <- g + ggplot2::scale_y_continuous(limits = c(ymin, ymax), labels = labels, sec.axis = ggplot2::dup_axis(), oob = scales::rescale_none)

        g <- g + ggplot2::guides(color = "none", fill = "none")

    }

    if(zero_line) g <- g + ggplot2::geom_abline(slope = 0, intercept = 0)

    g <- g + ggplot2::labs(title = title, subtitle = subtitle, caption = caption) + ggplot2::xlab(NULL) + ggplot2::ylab(NULL)

    if(!is.null(plot_colors)) {
        g <- g + ggplot2::scale_fill_manual(name = "variable", values = plot_colors, labels = plot_names)
        g <- g + ggplot2::scale_color_manual(name = "variable", values = plot_colors, labels = plot_names)
    } else {
        g <- g + ggplot2::scale_fill_discrete(name = "variable", labels = plot_names)
        g <- g + ggplot2::scale_color_discrete(name = "variable", labels = plot_names)
    }

    if(show_plot) print(g)
    if(save_plot) assign(make.names(paste("g", tolower(title))), g, envir = globalenv())

    g
}
